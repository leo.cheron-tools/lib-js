import PageEvent from "lib/anonymous/pages/events/PageEvent";
import Console from "lib/anonymous/utils/Console";
import Config from "Config";
import Emitter from "Emitter";

export default class APage extends Emitter
{
	constructor(dom, previousPage)
	{
		super();

		this.id = dom.getAttribute("id") + "-" + ((Math.random() * 100000) | 0 / 10000);
		this.sitemap = window.sitemap;
		
		this.$ = $(dom);

		if(previousPage && !previousPage.hidden)
		{
			// previous page is not hidden yet
			this._previousPage = previousPage;
			this._previousPage.on(PageEvent.HIDDEN, this._onPreviousPageHidden.bind(this));
		}
	}

	/**
	 * @public
	 * To be overridden with super
	 */
	init()
	{
		Console.color(Console.GREEN).log("init page", this.id);
	}

	/**
	 * @public
	 */
	resize()
	{
	}

	/**
	 * @public
	 */
	update()
	{
	}

	/**
	 * @public
	 * To be overridden with super
	 */
	destroy()
	{
		Console.color(Console.RED).log("destroy page", this.id);

		this.$.off();
		this.off();

		this.$.remove();
	}

	//-----------------------------------------------------o show / hide logic

	/**
	 * @protected
	 * To be overridden with super if needed
	 */
	_onPreviousPageHidden(target)
	{
		this._previousPage = null;
	}

	/**
	 * Must be overriden with super
	 * Call this._shown() if no transition
	 */
	show()
	{
		Console.color(Console.BLUE).log("show page", this.id);
		
		this.resize();

		this.hidden = false;
		
		this.emit(PageEvent.SHOW, this);

		this._show();
	}

	_show() 
	{
		// tansition
	}

	/**
	 * Call this method on show() transition end
	 */
	_shown()
	{
		this.shown = true;

		this.emit(PageEvent.SHOWN, this);
	}

	/**
	 * Must be overriden with super
	 * Call this._hidden() if no transition
	 */
	hide()
	{
		Console.color(Console.ORANGE).log("hide page", this.id);

		this.shown = false;

		this.emit(PageEvent.HIDE, this);

		this._hide();
	}

	_hide() 
	{
		// tansition
	}

	/**
	 * @protected
	 * Call this method on hide() transition end
	 */
	_hidden()
	{
		this.hidden = true;

		this.emit(PageEvent.HIDDEN, this);
	}
}