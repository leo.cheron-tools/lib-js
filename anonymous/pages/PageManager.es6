import Config from "Config";
import Router from "lib/anonymous/core/router/Router";
import RouterEvent from "lib/anonymous/core/router/RouterEvent";
import PageEvent from "lib/anonymous/pages/events/PageEvent";
import Emitter from "Emitter";

/**
 * PageManager
 * @constructor
 */
export default class PageManager extends Emitter
{
	constructor()
	{
		super();

		this._router = new Router();

		this.init();
	}

	init()
	{
		this._$content = $("#container");
		var $dom = this._$content.children().last();
		PageManager.currentId = $dom.attr("id");

		this._router.on(RouterEvent.CHANGE, this._onStateChange.bind(this));

		this._ajaxify($dom[0]);
	}

	update()
	{
		if(this._page)
			this._page.update();

		if(this._previousPage)
			this._previousPage.update();
	}

	resize()
	{
		if(this._page)
			this._page.resize();

		if(this._previousPage)
			this._previousPage.resize();
	}

	//-----------------------------------------------------o private

	_ajaxify(dom)
	{
		this._previousPage = this._page;
		if(this._previousPage)
		{
			this._previousPage.on(PageEvent.HIDDEN, this._onPageHidden.bind(this));
			this._previousPage.hide();

			window.scrollTo(0, 0);
		}

		var PageClass;
		if(window.sitemap.pages[PageManager.currentId])
			PageClass = window.sitemap.pages[PageManager.currentId].class;
		else
			PageClass = window.sitemap.pages["default"].class;
		
		this._page = new PageClass.default(dom, this._page);
		this._page.init();
		this._page.on(PageEvent.SHOWN, this._onPageShown.bind(this));
		this._page.show();
	}

	_setContent(data)
	{
		var $data = $(data),
			$content = $data.filter('#container');

		var dom = $content.children().last()[0];
		this._$content.append(dom);

		PageManager.currentId = this._$content.children().last().attr("id");

		Router.setTitle($data.filter('title').text());

		this._ajaxify(dom);
	}

	//-----------------------------------------------------o Handlers

	_onStateChange()
	{
		var State = History.getState();

		if(this._page)
			this._page.scrollLocked = true;

		if(!State.data.silent)
		{
			if(this._xhr)
				this._xhr.abort();

			this._xhr = $.ajax
			({
				url: State.url,
				type: 'GET',
				xhrFields:
				{
					withCredentials: true
				},
				success: (data) =>
				{
					this._router.locked = true;

					this._setContent(data);
					this._xhr = null;
				},
				error: function(xhr, type)
				{
					if(Config.DEBUG)
						console.log("PageManager xhr error", type, xhr);
				}
			});
		}
	}

	_onPageHidden(target)
	{
		if(this._page.shown)
		{
			this._previousPage.destroy();
			this._previousPage = null;
			this._router.locked = false;
		}
	}

	_onPageShown(target)
	{
		if(this._previousPage && this._previousPage.hidden)
		{
			this._previousPage.destroy();
			this._previousPage = null;
			this._router.locked = false;
		}
	}
}