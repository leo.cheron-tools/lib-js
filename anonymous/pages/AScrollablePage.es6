import APage from "./APage";
import Stage from "lib/anonymous/core/Stage";
import Scrollable from "lib/anonymous/display/Scrollable";

export default class AScrollablePage extends APage
{
	constructor(dom, previousPage)
	{
		super(dom, previousPage);
	}

	init()
	{
		super.init();

		this.scrollable = new Scrollable(this.$[0]);
	}

	resize()
	{
		super.resize();

		this.scrollable.resize();

		this.height = this.scrollable.height;
	}

	update(now = false)
	{
		super.update();

		this.scrollable.update();
	}

	destroy()
	{
		super.destroy();

		this.scrollable.destroy();
	}

	hide()
	{
		super.hide();

		this.scrollable.scrollLocked = true;
	}
}