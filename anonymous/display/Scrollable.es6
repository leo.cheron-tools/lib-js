import Stage from "lib/anonymous/core/Stage";
import MouseEvent from "lib/anonymous/events/MouseEvent";
import Css from "lib/anonymous/utils/Css";
import DisplayUtils from "lib/anonymous/utils/DisplayUtils";

export default class Scrollable
{
	constructor(dom)
	{
		this.dom = dom;

		this.id = Math.random();

		this.init();
	}

	init()
	{
		this.vy = 0;
		this.y = this._y = this._oy = window.scrollY || window.pageYOffset;

		this._easing = 0.75;
		this._friction = 0.2;

		this.scrollLocked = false;

		this._firstScroll = true;

		this._scrollify();
	}

	resize()
	{
		this.height = DisplayUtils.offset(this.dom).height;

		if (this._dummy)
			this._dummy.style.height = this.height + "px";
	}

	update(now = false)
	{
		if(!this.scrollLocked && this._y != undefined)
		{
			if(MouseEvent.touch)
			{
				if(this._dragging)
				{
					this.y += (this._y - this.y) * (now ? 1 : this._easing);

					var constraint = 0.63;
					if(this.y > 0)
						this.y -= this.y * constraint;
					else if (this.y < -(this.height - Stage.height))
						this.y += (-(this.height - Stage.height) - this.y) * constraint;

					this.vy = this.y - this._oy;
				}
				else
				{
					this.y += (this.vy *= this._friction);

					if(this.y > 0)
					{
						this.vy *= this._friction;
						this.y += -this.y * 0.2;
					}
					else if(this.y < -(this.height - Stage.height))
					{
						this.vy *= this._friction;
						this.y += (-(this.height - Stage.height) - this.y) * 0.2;
					}
				}
			}
			else
			{
				this.vy -= (this._y + this.y) * (now ? 1 : this._easing);
				this.y += (this.vy *= this._friction);
			}

			this.y = parseFloat(this.y.toFixed(4));

			if(this._oy !== this.y || now)
				this._updateDom();
			
			this._oy = this.y;
		}
	}

	destroy()
	{
		if(this._dummy)
			this._dummy.parentNode.removeChild(this._dummy);

		if(MouseEvent.touch)
		{
			Stage.$window
				.off('touchstart.' + this.id)
				.off('touchend.' + this.id)
				.off('touchmove.' + this.id);
		}
		else
		{
			Stage.$document.off('DOMMouseScroll.' + this.id + ' mousewheel.' + this.id + ' scroll.' + this.id);
		}
	}

	set enabled(value)
	{
		this.scrollLocked = !value;

		if(!value && this._dummy)
			this._dummy.style["display"] = "none";
		else if(this._dummy)
			this._dummy.style["display"] = "block";
	}

	//-----------------------------------------------------o private

	_updateDom()
	{
		Css.transform(this.dom, "translate3d(0," + (MouseEvent.touch ? this.y | 0 : this.y) + "px,0)");
	}

	_scrollify()
	{
		// update dom to make things work
		this.dom.style['position'] = "fixed";
		this.dom.style['z-index'] = 1;
		this.dom.classList.add("scrollable");
		this.dom.willChange = "transform";
		
		if(MouseEvent.touch)
		{
			Stage.$window
				.on('touchstart.' + this.id, this._onTouchDown.bind(this))
				.on('touchend.' + this.id, this._onTouchUp.bind(this))
				.on('touchmove.' + this.id, this._onTouchMove.bind(this));
		}
		else
		{
			this._dummy = document.createElement("div");
			this._dummy.style.position = "absolute";
			this._dummy.style.top = 0;
			this._dummy.style.width = "100%";
			this._dummy.style['z-index'] = 0;

			this.dom.parentNode.appendChild(this._dummy);
			Stage.$document
				.on("DOMMouseScroll." + this.id + " mousewheel." + this.id, this._onMouseScroll.bind(this))
				.on("scroll." + this.id , this._onScroll.bind(this));
		}
	}

	//-----------------------------------------------------o shared handlers

	_onMouseScroll(e)
	{
		// to override

		this._onScroll(e);
	}

	_onScroll(e)
	{
		if(!this.scrollLocked)
		{
			this._y = window.scrollY || window.pageYOffset;

			if(this._firstScroll)
			{
				this.y = this._oy = -this._y;
				this._firstScroll = false;
				this.update(true);
			}
		}
	}

	//-----------------------------------------------------o touch handlers

	_onTouchDown(e)
	{
		if(!this.scrollLocked)
		{
			var changedTouches = e.changedTouches[0] || e.touches[0];
			this._touchInitY = changedTouches.pageY;
			this._initY = this._y = this.y;

			this._dragging = true;

			this._easing = 0.9;
			this._friction = 0.9;
		}
	}

	_onTouchMove(e)
	{
		if(!this.scrollLocked)
		{
			e.preventDefault();

			e = e.changedTouches || e.touches;

			this._y = e[0].pageY - this._touchInitY + this._initY;
		}
	}

	_onTouchUp(e)
	{
		if(!this.scrollLocked)
		{
			this._dragging = false;
			this._friction = 0.96;
		}
	}
}