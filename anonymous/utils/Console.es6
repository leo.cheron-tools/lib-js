export default class Console
{
	constructor() {}

	static color(value)
	{
		Console.COLOR = value;

		return Console;
	}

	static log(...args)
	{
		var style = "font-size:" + Console.FONT_SIZE + "px;font-family:" + Console.FONT_FAMILY + ";";
		if(Console.COLOR) style += "color:" + Console.COLOR + ";";

		console.log("%c" + args.join(" "), style);

		Console.reset();
	}

	static reset()
	{
		Console.COLOR = null;
	}
}

// style
Console.FONT_FAMILY = "courier new";
Console.FONT_SIZE = 15;

// colors
Console.GREEN = "#8fff8c";
Console.BLUE = "#6fdbff";
Console.YELLOW = "#fffb8f";
Console.ORANGE = "#ffc06f";
Console.RED = "#ff7d7d";