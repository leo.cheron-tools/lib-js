"use strict";

var DisplayUtils = module.exports = function(){};

DisplayUtils.offset = function(element)
{
	var rect = element.getBoundingClientRect(), bodyElt = document.body;
	
	return {
		top: rect.top + bodyElt.scrollTop,
		right: rect.left + bodyElt.scrollRight,
		bottom: rect.left + bodyElt.scrollBottom,
		left: rect.left + bodyElt.scrollLeft,
		width: rect.width,
		height: rect.height
	};
};

DisplayUtils.detectContain = function(container, elem)
{
	return ((elem.top > container.top && elem.top < container.bottom) && (elem.bottom > container.top && elem.bottom < container.bottom));
};