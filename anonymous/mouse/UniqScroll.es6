import Emitter from "Emitter";
import Stage from "lib/anonymous/core/Stage";

export default class UniqScroll extends Emitter
{
	/**
	 * @param delay ms
	 */
	constructor(delay = 0) 
	{
		super();
		
		this._value = 0;
		this._locked = false;
		this._allowUnlock = true;

		this._delay = delay;

		this._bind();
	}

	_bind() 
	{
		Stage.$window.on('mousewheel.step DOMMouseScroll.step wheel.step MozMousePixelScroll.step', this._onScroll.bind(this));
		Stage.$window.on('touchstart', this._onTouch.bind(this));

		this.throttleDown = this.emit.bind(this, 'scroll', {direction: 'down'});
		this.throttleUp   = this.emit.bind(this, 'scroll', {direction: 'up'});
	}

	_onTouch(e)
	{
		console.log("UniqScroll TOUCH");
	}

	destroy()
	{
		Stage.$window.off('mousewheel.step DOMMouseScroll.step wheel.step MozMousePixelScroll.step');
		this.off();
	}

	_onScroll(e) 
	{
		e = e || window.event;

		var value = e.wheelDelta || -e.deltaY || -e.detail;
		var delta = value > 0 ? 1 : -1;
		var ov = this._value > 0 ? this._value : -this._value;
		var v = value > 0 ? value : -value;

		// only trackpad send a 0 value when released
		if(!value)
		{
			this._locked = true;
			this._allowUnlock = false;
			this._trackPad = true;
		}
		else if(v > ov && this._allowUnlock)
		{
			this._locked = false;
		}

		// velocity decreases and trackpad is used
		if(this._trackPad && v < ov)
		{
			this._locked = true;
			this._allowUnlock = true;
		}

		if(value)
			this._value = value;

		if (!this._locked && !this._delayLocked) 
		{
			if (delta < 0) 
				this.throttleDown();
			else 
				this.throttleUp();

			if(this._delay)
			{
				this._delayLocked = true;
				if(this._timeout) clearTimeout(this._timeout);
				this._timeout = setTimeout(() => 
				{
					this._delayLocked = false;
				}, this._delay);
			}
		}
	}
}