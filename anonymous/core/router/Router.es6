/* globals History, ga */
import Config from "Config";
import Stage from "lib/anonymous/core/Stage";
import MouseEvent from "lib/anonymous/events/MouseEvent";
import RouterEvent from "./RouterEvent";
import Emitter from "Emitter";
import history from "lib/zepto/zepto.history";

/**
 * Router
 * @static
 */
export default class Router
{
	constructor()
	{
		Emitter(this);

		Stage.$body.on(MouseEvent.CLICK + ".router", 'a:not([target])', this._onClickLink.bind(this));

		Stage.$window.on('statechange', this._onStateChange.bind(this));

		this.parser = document.createElement('a');
		this.url = window.location.href;
		if(window.location.hash !== "")
			this.url = this.url.replace("/" + window.location.hash, "").replace(window.location.hash, "");
		this.origin = window.location.origin;

		History.replaceState(null, document.title, this.url); // Override previousId if defined (eg: after a refresh)
	}

	//-----------------------------------------------------o public

	static setTitle(title)
	{
		document.title = title;
	}

	static setCurrentId(id)
	{
		Router.currentId = id;
	}

	//-----------------------------------------------------o handlers

	_onClickLink(e)
	{
		if(e.button !== 1)
		{
			e.preventDefault();

			this.parser.href = e.currentTarget.getAttribute('href');

			let forwardSlash = '';
			if (this.parser.pathname[0] !== '/') // IE 11
				forwardSlash = '/';

			let href = this.origin + forwardSlash + this.parser.pathname + this.parser.search + this.parser.hash;

			History.pushState({previousId: Router.currentId}, Config.TITLE, href);

			if(window.ga)
				ga('send', 'pageview');
		}
	}

	_onStateChange()
	{
		var url = window.location.href.replace("/" + window.location.hash, "").replace(window.location.hash, "");
		if(this.url !== url)
		{
			var state = History.getState();
			if(!state.data.prevented)
			{
				this.emit(RouterEvent.CHANGE, state.url);
				
				Router.previousUrl = Router.url;
				Router.url = state.url;
			}
		}
		this.url = url;
	}
}