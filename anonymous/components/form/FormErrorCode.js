var FormErrorCode = module.exports = function() {};
// custom
FormErrorCode.REQUIRED = 1;
FormErrorCode.INVALID = 2;
FormErrorCode.PWD_NO_MATCH = 3;
FormErrorCode.EXISTS = 4;