/* globals require, FormEvent, module */
"use strict"; // jshint ignore: line

var Input = require('lib/anonymous/components/input/Input');
var InputBirthdate = require('lib/anonymous/components/input/InputBirthdate');
var InputSelect = require('lib/anonymous/components/input/InputSelect');

/**
 * Form
 * @constructor
 */
var Form = module.exports = function ($form)
{
	this.$ = $(this);
	
	this.$form = $form;
	this._$inputs = this.$form.find('input, select');
	
	this._init();
};

Form.SUBMIT = 'formsubmit';
Form.ERROR = 'formerror';

Form.prototype =
{
	_init: function()
	{
		this.$form.on(FormEvent.SUBMIT, $.proxy(this._onSubmit, this));

		this._inputs = [];
		for (var i = this._$inputs.length - 1; i >= 0; i--)
		{
			var $input, klass;
			$input = this._$inputs.eq(i);
			switch( $input.attr('type').toLowerCase() )
			{
				case 'date':
					klass = new InputBirthdate( $input );
					break;
				case 'select-one':
					klass = new InputSelect( $input );
					break;
				case 'radio':
    				/* falls through */
				default:
					klass = new Input( $input );
					break;
			}

			if (klass)
				this._inputs.push( klass );
		}

	},

	validate: function()
	{
		var valid = true;

		for (var i = this._inputs.length - 1; i >= 0; i--)
			if ( !this._inputs[i].isValid())
				if (valid) valid = false;

		return valid;
	},

	destroy: function()
	{
		for (var i = this._inputs.length - 1; i >= 0; i--)
			this._inputs[i].destroy();
	},

	_beforeSend: function()
	{
		for (var i = this._inputs.length - 1; i >= 0; i--)
			this._inputs[i]._beforeSend();
	},

	_afterSend: function()
	{
		for (var i = this._inputs.length - 1; i >= 0; i--)
			this._inputs[i]._afterSend();
	},

	//-----------------------------------------------------o Getter

	get: function(name)
	{
		for (var i = this._inputs.length - 1; i >= 0; i--)
		{
			if (this._inputs[i].name === name) return this._inputs[i];
		}
	},

	//-----------------------------------------------------o Handlers

	_onSubmit: function(e)
	{
		e.preventDefault();
		
		if ( this.validate() )
		{
			this._beforeSend();
			$.ajax({
				url: this.$form.prop('action'),
				type: this.$form.prop('method'),
				data: this.$form.serialize(),
				xhrFields: 
				{
					withCredentials: true
				},
				success: $.proxy(function(result)
				{
					if (result.success)
					{
						this.$.trigger(Form.SUBMIT);
					} else {
						// console.log(result.error);

						for (var i = 0; i < result.error.length; i++)
						{
							var input = this.get(result.error[i].field);
							// console.log(input);
							
							if (input)
							{
								input.addError();
								input.addTextError(result.error[i].error_code);
							}

						}

						// console.log(result);
					}
				}, this)
			});
			this._afterSend();
		}
		else
		{
			// this.$.trigger(Form.ERROR);
		}
	},
};