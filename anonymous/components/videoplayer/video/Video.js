/* globals module, require */
"use strict"; // jshint ignore: line

// var Css = require("lib/utils/Css");
var VideoEvent = require("../events/VideoEvent");

/**
 *	@param $dom: $("video")
 */
var Video = module.exports = function($dom)
{
	this.$ = $(this);
	this.$dom = $dom;
	this.id = Math.random();

	this.muted            = false;
	this.volumeBeforeMute = 0;

	this.init();
};

Video.prototype =
{
	init: function()
	{
		this._currentTime     = 0;
		this.ready            = false;
		this.paused           = true;

		this.$dom
			.on('error', $.proxy(this._onError, this))
			.on('loadedmetadata', $.proxy(this._onMetadata, this));

		this.video = this.$dom[0];
	},

	destroy: function()
	{
		this.$.off();
		this.$dom.off();
	},

	_play: function()
	{
		this.video.play();
	},

	play: function (e)
	{
		this.paused = false;
		this.ended = false;

		this.$.trigger(VideoEvent.PLAY);

		this._play();
	},

	_pause: function()
	{
		this.video.pause();
	},

	pause: function()
	{
		if(!this.paused)
		{
			this._pause();
			this.$.trigger(VideoEvent.PAUSE);
		}

		this.paused = true;
	},

	stop: function()
	{
		this.paused = true;

		this.seek(0.1); //fix for iOS
	},

	togglePause: function(e)
	{
		if(e)
		{
			e.preventDefault();
			e.stopImmediatePropagation();
		}

		if(this.paused)
			this.play();
		else
			this.pause();
	},

	toggleMute: function(e)
	{
		if(e)
		{
			e.preventDefault();
			e.stopImmediatePropagation();
		}

		if(this.volume() === 0)
		{
			this.volume(this.volumeBeforeMute);
		}
		else
		{
			this.volumeBeforeMute = this.volume();
			this.volume(0);
		}
	},

	/**
	 * @param time [seconds]
	 */
	seek: function(time)
	{
		this._currentTime = time;
		if(this.duration() > 0)
		{
			this.currentTime(time);
		}
		else
		{
			this._toSeek = time;
			if(!this.paused) // resume video
				this.play();
		}
	},

	//-----------------------------------------------------------------o Getters / Setters

	/**
	 * Video currentTime
	 * @param time [seconds]
	 */
	currentTime: function(value)
	{
		if (value)
			this.video.currentTime = value;
		else
			return this.video.currentTime;
	},

	/**
	 * Video duration
	 */
	duration: function(value)
	{
		if(value) console.warn("duration is read only");
		return this.video.duration;
	},

	/**
	 * Video buffer
	 */
	buffer: function(value)
	{
		if(value) console.warn("buffer is read only");
		return this.video.buffer;
	},

	/**
	 * Set a new video source, webm and mp4.
	 * @param value		File path without extension
	 */
	src: function(value)
	{
		this._progressed = false;
		this._toSeek = null;

		this.paused = false;

		this.$dom.off();
		this.$dom.on(VideoEvent.ERROR, $.proxy(this._onError, this));
		this.$dom.on(VideoEvent.LOADED_METADATA, $.proxy(this._onMetadata, this));
		this.$dom.html('<source src="' + value + '.webm" type="video/webm"><source src="' + value + '.mp4" type="video/mp4">');

		this.video.load();
	},

	/**
	 * Set volume
	 * @param value [0 - 1]
	 */
	volume: function(value)
	{
		if(typeof value === 'undefined')
		{
			if(value >= 0 && value <= 1)
				this.video.volume = value;

			if(value === 0)
			{
				this.muted = true;
				this.$.trigger(VideoEvent.SOUND_MUTE);
			}

			if(value > 0 && value <= 1 && this.muted)
			{
				this.muted = false;
				this.$.trigger(VideoEvent.SOUND_UNMUTE);
			}
		}
		else
		{
			return this.video.volume;
		}
	},

	//-----------------------------------------------------o Video handlers

	_onError: function(e)
	{
		console.log("Video ERROR", e, this.currentTime, this._currentTime, this.paused);
	},

	_onMetadata: function()
	{
		this.$.off('loadedmetadata', $.proxy(this._onMetadata, this));

		this.ready = true;

		this.$dom
			.on('canplaythrough.' + this.id, $.proxy(this._onCanplaythrough, this))
			.on('progress.' + this.id, $.proxy(this._onProgress, this))
			.on('waiting.' + this.id, $.proxy(this._onWaiting, this))
			.on('stalled.' + this.id, $.proxy(this._onWaiting, this))
			// .on('play.' + this.id, $.proxy(this._onPlay, this))
			.on('pause.' + this.id, $.proxy(this._onPause, this))
			.on('ended.' + this.id, $.proxy(this._onEnded, this))
			.on('readystatechange.' + this.id, $.proxy(this._onReadyStateChange, this));

		if(this._toSeek && !isNaN(this._toSeek)) // video was to be seeked before being ready
		{
			var num = parseFloat(this._toSeek);
			if(num === 0) num = 0.1;
			this.seek(parseFloat(num.toFixed(1)));
		}

		this.$.trigger(VideoEvent.LOADED_METADATA);
	},

	_onCanplaythrough: function()
	{
		if(this._currentTime > 0)
			this.currentTime(this._currentTime);

		this.$.trigger(VideoEvent.READY);
	},

	_onProgress: function()
	{
		if(this.video.buffered.length === 0)
			this.currentBuffer = 0;
		else
			this.currentBuffer = this.video.buffered.end(0);

		this.buffered = this.currentBuffer - this.video.currentTime;
		this.loaded = this.currentBuffer / this.video.duration;
		if(this.loaded > 1) this.loaded = 1;

		this._currentTime = this.video.currentTime;
		
		/*if(this._canPlayThrough && this.currentBuffer > 1 && !this._progressed)
		{
			this._progressed = true;
			this.bind.trigger(Video.READY);
		}*/

		/*if(this.buffered > 5 || this.loaded > 0.6)
		 {
		 this.bind.trigger(Video.READY);
		 }*/

		// TODO test on ff 3.6
		/*else if (this.video.bytesTotal != undefined && this.video.bytesTotal > 0 && this.video.bufferedBytes != undefined)
		 loaded = this.video.bufferedBytes / this.video.bytesTotal;*/
	},

	_onReadyStateChange: function() 
	{
		// 
	},

	_onWaiting: function() 
	{
		this.$.trigger(VideoEvent.BUFFERING);
	},

	_onPlay: function()
	{
		this.$.trigger(VideoEvent.PLAY);
	},

	_onPause: function()
	{
		this.$.trigger(VideoEvent.PAUSE);
	},

	_onEnded: function()
	{
		console.log("Video", this.ended );
		if(!this.ended)
			this.$.triggerHandler(VideoEvent.ENDED);

		this.ended = true;
	}
};