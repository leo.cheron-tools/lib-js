/* globals YT, module, require */
"use strict"; // jshint ignore: line

var Video = require("./Video");
var VideoEvent = require("../events/VideoEvent");

/**
 * VideoYT
 * @constructor
 */
var VideoYT = function($dom)
{
	Video.apply(this, arguments);
};

VideoYT.prototype = $.extend({}, Video.prototype,
{
	init: function()
	{
		this._currentTime = 0;
		this.volume(1);

		this.ready = false;
		this.paused = true;
		this.ytPaused = true;

		this._prevt = -1;

		this.idYT = this.$dom.data('id');
	},

	initYT: function(videoId)
	{
		if(!this.video)
		{
			this.video = new YT.Player('player-' + this.idYT, 
			{
				width: '100%',
				height: '100%',
				videoId: videoId || this.idYT,
				// suggestedQuality: 'hd720',
				playerVars:
				{
					modestbranding: 1,
					autoplay: MouseEvent.touch ? 0 : 1,
					// autoplay: 0,
					controls: 0,
					loop: 0,
					showinfo: 0,
					rel: 0,
					html5: 1,
					wmode: "opaque",
					// cc_load_policy: 1,
					// cc_lang_pref: 'en'
				},
				events:
				{
					'onReady': $.proxy(this._onPlayerReady, this),
					'onStateChange': $.proxy(this._onPlayerStateChange, this),
					//'onPlaybackQualityChange': this._onPlaybackQualityChange
				}
			});

			ga('send', 'event', 'video', 'play', videoId || this.idYT);
		}
	},

	destroy: function()
	{
		if(this.video)
			this.video.destroy();
	},

	_play: function()
	{
		this.video.playVideo();
		// this.volume(0); //DEBUG
	},

	_pause: function()
	{
		this.video.pauseVideo();
	},

	stop: function()
	{
		//this.video.stopVideo();
		this.pause();
		//this.video.seekTo(0.1);
	},

	//-----------------------------------------------------------------o Getters / Setters

	/**
	 * Video currentTime
	 * @param time [seconds]
	 */
	currentTime: function(value)
	{
		if (value !== undefined)
			this.video.seekTo(value);
		else if(this.ready)
		{
			var vt, t;
			vt = this.video.getCurrentTime();
			if(vt === this._prevt && this._dt >= 0 && !this.paused && !this.ytPaused)
			{
				var dt = new Date().getTime() - this._dt;
				t = vt + dt * 0.001;
			}
			else
			{
				this._dt = new Date().getTime();
				t = vt;
			}

			this._prevt = vt;

			if(this.duration() - t < 0.01)
			{
				this.paused = true;
				this._onEnded();
			}

			return t;
		}
		else
			return 0;
	},

	/**
	 * Video duration
	 */
	duration: function(value)
	{
		if(value) console.warn("duration is read only");
		if(this.ready)
			return this.video.getDuration();
		else
			return -1;
	},

	/**
	 * Video buffer
	 */
	buffer: function(value)
	{
		if(value) console.warn("buffer is read only");
		return this.video.buffer;
	},

	src: undefined,

	/**
	 * Set volume
	 * @param value [0 - 1]
	 */
	volume: function(value)
	{
		if(typeof value !== 'undefined')
		{
			if(!this.ready) { return; }

			if(value >= 0 && value <= 1)
			{
				this.video.setVolume( parseInt(value * 100, 10));

				if(value === 0)
				{
					this.muted = true;
					this.$.trigger(VideoEvent.MUTE);
				}

				if(value > 0 && value <= 1 && this.muted)
				{
					this.muted = false;
					this.$.trigger(VideoEvent.UNMUTE);
				}
			}
		}
		else
		{
			if(!this.ready){ return; }
			return parseInt(this.video.getVolume() / 100, 10);
		}
	},

	/**
	 * Set quality
	 * @param value [string]
	 */
	quality: function(value)
	{
		if (value)
		{
			this.video.stopVideo();
			this.video.setPlaybackQuality(value);
			if(this.paused)
				this._willPause = true;
			this.play();
		}
		else
			this.video.getPlaybackQuality();
	},

	loadVideoById: function(id, startSeconds, suggestedQuality)
	{
		this.ready = false;
		
		if(this.video)
		{
			// this.pause();
			// this._willPause = true;
			this.video.loadVideoById(id, startSeconds, suggestedQuality);
		}
		else
		{
			this.initYT(id);
		}
	},

	//-----------------------------------------------------------------o VideoYT handlers

	_onPlayerReady: function()
	{
		// TODO getAvailableQualityLevels;
		this.$dom = $('#player-' + this.idYT);

		this.ready = true;

		this.video.setPlaybackQuality("hd720");
		// this.video.setVolume(this.volume());
		// this.player.loadModule("captions");  //Works for html5 ignored by AS3
		// this.player.loadModule("cc");
		
		this.$.trigger(VideoEvent.READY);

		// auto play
		if(!this.paused && !MouseEvent.touch)
			this.play();
	},

	_onPlayerStateChange: function(e)
	{
		// console.log("YTVideo", e.data, YT.PlayerState.PLAYING, YT.PlayerState.ENDED);
		switch(e.data)
		{
			case YT.PlayerState.PLAYING:
				this.ready = true;
				this._prevt = -1;
				this.ytPaused = false;
				this.paused = false;
				this.ended = false;
				
				if (this._willPause)
				{
					this._willPause = false;
					this.pause();
				}
				this._onPlay();
				break;

			case YT.PlayerState.PAUSED:
				this.ytPaused = true;
				this.paused = true;
				this._onPause();
				break;

			case YT.PlayerState.ENDED:
				this._onEnded();
				this.ytPaused = true;
				break;
		}
	},

});

if (module) module.exports = VideoYT;