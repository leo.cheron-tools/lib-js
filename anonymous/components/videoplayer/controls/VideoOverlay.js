/* globals module */
"use strict"; // jshint ignore: line

var Easing = require('lib/zepto/Easing');

/**
 * VideoOverlay
 * @constructor
 */
var VideoOverlay = module.exports = function($dom, params)
{
	this.$ = $(this);
	this.$dom = $dom;

	$.extend(this, {poster: true}, params);
	this.init();
};

VideoOverlay.prototype =
{
	init: function()
	{
		this.$dom.on(MouseEvent.CLICK, $.proxy(this._onClick, this));
		this.$dom.on(MouseEvent.ENTER, $.proxy(this._onEnter, this));
		this.$dom.on(MouseEvent.LEAVE, $.proxy(this._onLeave, this));

		this._$btnPlayPause = this.$dom.find('.btn--play-pause');

		this._$img = this.$dom.find('.video__overlay__poster');

		if(this.poster)
		{
			var img = document.createElement("img");
			img.className = "video__overlay__poster";
			img.src = this._$img.attr("data-src");

			this._$img.replaceWith(img);
			this._$img = this.$dom.find('.video__overlay__poster');
		}

		// if(MouseEvent.touch)
		// 	this.$dom.css("pointer-events", "none");
	},

	destroy: function()
	{
		this.$.off();
		this.$dom.off();
	},

	show: function()
	{
		this.hidden = false;

		this._$img.show();

		this.$dom.css({cursor: "pointer", zIndex: 15});
	},

	hide: function()
	{
		this.hidden = true;

		this._$img.hide();

		this.$dom.css({cursor: "auto", zIndex: 5});
	},

	showControls: function()
	{
		this._$btnPlayPause.animate({'opacity':1}, 150, Easing.outQuad);
	},

	hideControls: function()
	{
		this._$btnPlayPause.animate({'opacity':0}, 500, Easing.outQuad);
	},

	togglePlayPauseTo: function(value)
	{
		this._$btnPlayPause.find('.icon').removeClass().addClass('icon icon--player-'+value);
	},

	//-----------------------------------------------------o Getters & Setters

	cover: function(src)
	{
		this._$img.attr('src', src);
	},

	//-----------------------------------------------------o Handlers

	_onClick: function(e)
	{
		// TODO loading feedback while video init
		this.$.triggerHandler(MouseEvent.CLICK);
	},
	_onEnter: function(e)
	{
		this.$.trigger(MouseEvent.ENTER);
	},
	_onLeave: function(e)
	{
		this.$.trigger(MouseEvent.LEAVE);
	},
};