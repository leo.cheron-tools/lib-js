"use strict"; // jshint ignore: line

var Css = require("lib/anonymous/utils/Css");
var VideoEvent = require("../events/VideoEvent");
var Easing = require('lib/zepto/Easing');

/**
 * VideoControls
 * @constructor
 * @params 	dom 	jquery controller dom element
 * 			video 	jquery video element
 */
var VideoControls = module.exports = function(dom)
{
	this.id = Math.random();
	this.$ = $(this);
	this.$dom = dom;

	this.init();
};

VideoControls.prototype =
{
	init: function()
	{
		this._$timer = this.$dom.find(".video__timer");

		this._$btnVolume = this.$dom.find(".btn--mute");
		this._$btnVolume.on(MouseEvent.CLICK, $.proxy(this._onToggleMute, this));

		this._$btnPlayPause = this.$dom.find(".btn--play-pause");
		this._$btnPlayPause.on(MouseEvent.CLICK, $.proxy(this._onTogglePause, this));

		this._$btnFullscreen = this.$dom.find(".btn--fullscreen");
		this._$btnFullscreen.on(MouseEvent.CLICK, $.proxy(this._onToggleFullscreen, this));

		this._$timeline = this.$dom.find(".video__timeline");
		this._$timeline.on(MouseEvent.DOWN, $.proxy(this._onTimelineDown, this));

		this._$bufferBar = this.$dom.find(".video__buffer");
		this._$progressBar = this.$dom.find(".video__progress");

	},

	destroy: function()
	{
		this._$btnPlayPause.off();
		this._$btnFullscreen.off();
		this._$timeline.off();
	},

	show: function()
	{
		this.$dom.animate({/*translate3d: "0,0,0", */visibility: "visible", opacity: 1}, 800, Easing.outExpo);
	},

	hide: function()
	{
		this.$dom.animate({visibility: "hidden", opacity: 1}, 800, Easing.outExpo);
	},

	minify: function()
	{
		/*translate3d: "0,70%,0", */
		this.$dom.animate({opacity: 0.2}, 1600, Easing.outExpo);
	},

	unminify: function()
	{
		/*translate3d: "0,0,0", */
		this.$dom.animate({opacity: 1}, 800, Easing.outExpo);
	},

	reset: function()
	{
		this.time(0, -1);
		this.$dom.animate({/*translate3d: "0,100%,0", */visibility: "hidden", opacity: 0}, 600, Easing.outExpo);
	},

	//-----------------------------------------------------o Getters / setters

	time: function(value, total)
	{
		var min = value / 60 | 0;
		if(min < 10) min = "0" + min;
		var sec = value % 60 | 0;
		if(sec < 10) sec = "0" + sec;
		var st = min + ":" + sec;

		if(st !== this._st)
		{
			this._$timer.text(st);
			this._st = st;
		}

		Css.transform(this._$progressBar[0], "scale(" + (value / total) + ",1)");

		// TODO manage buffering
		// this.buffered = this._player.getVideoLoadedFraction();
		// this.buffered = this.buffer();

		// if (this.buffered <= 1)
		// 	Css.transform(this._$bufferBar[0], "scale(" + this.buffered + ",1)");
	},

	mode: function(value)
	{
		if(value === "playing")
			this._$btnPlayPause.find('.icon').removeClass().addClass("icon icon--player-pause");
		else
			this._$btnPlayPause.find('.icon').removeClass().addClass("icon icon--player-play");
	},

	mute: function(value)
	{
		if(value === "on")
			this._$btnVolume.find('.icon').removeClass().addClass("icon icon--player-sound-off");
		else
			this._$btnVolume.find('.icon').removeClass().addClass("icon icon--player-sound-on");
	},

	hideForMobile: function()
	{
		this._$btnPlayPause.hide();
		this._$btnVolume.hide();
	},

	//-----------------------------------------------------o Controller handlers

	_onTogglePause: function(e)
	{
		this.$.trigger(VideoEvent.TOGGLE_PLAY_PAUSE);
	},

	_onToggleMute: function(e)
	{
		this.$.trigger(VideoEvent.TOGGLE_MUTE);
	},

	_onToggleFullscreen: function(e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();

		this.$.trigger(VideoEvent.TOGGLE_FULLSCREEN);
	},

	_onTimelineDown: function(e)
	{
		if (e)
		{
			e.preventDefault();
			e.stopImmediatePropagation();
		}

		this.dragging = true;

		this.$.triggerHandler(VideoEvent.TIMELINE_DOWN);

		$(window)
			.on(MouseEvent.MOVE + "." + this.id, $.proxy(this._onTimelineMove, this))
			.on(MouseEvent.UP + "." + this.id, $.proxy(this._onTimelineUp, this));

		this._onTimelineMove(e);
	},

	_onTimelineMove: function(e,f)
	{
		e.preventDefault();

		var x = MouseEvent.touch ? e.touches[0].pageX : e.pageX;

		this.progress = (x - this._$timeline.offset().left) / this._$timeline.width();
		if(this.progress < 0) this.progress = 0;
		else if(this.progress > this.buffered) this.progress = this.buffered;

		this.$.triggerHandler(VideoEvent.SEEKING, [this.progress]);
	},

	_onTimelineUp: function(e)
	{
		e.stopImmediatePropagation();
		e.preventDefault();

		$(window)
			.off(MouseEvent.CLICK + "." + this.id)
			.off(MouseEvent.MOVE + "." + this.id)
			.off(MouseEvent.UP + "." + this.id);

		this.$.triggerHandler(VideoEvent.TIMELINE_UP);

		setTimeout($.proxy(function() { this.dragging = false; }, this), 0);
	}
};