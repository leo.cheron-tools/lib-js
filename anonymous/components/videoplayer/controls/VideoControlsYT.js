"use strict"; // jshint ignore: line

var VideoControls = require("./VideoControls");
var VideoEvent = require("../events/VideoEvent");
var SocialShare    = require('lib/anonymous/utils/social/SocialShare');
var Easing = require('lib/zepto/Easing');

/**
 * VideoControlsYT
 * @constructor
 */
var VideoControlsYT = module.exports = function()
{
	VideoControls.apply(this, arguments);
};

VideoControlsYT.prototype = $.extend({}, VideoControls.prototype,
{
	init: function()
	{
		VideoControls.prototype.init.call(this);

		this._$btnQualitySelector = this.$dom.find(".video__controller__quality");
		this._$btnQualitySelector.on(MouseEvent.CLICK, $.proxy(this._onClickQualitySelector, this));

		this._$btnQualities = this.$dom.find(".video__controller__qualities li");
		this._$btnQualities.on(MouseEvent.CLICK, $.proxy(this._onClickQuality, this));
		
		this._$btnFacebook = this.$dom.find('.btn--share-facebook');
		this._$btnFacebook.on(MouseEvent.CLICK, function() { ga("send", "event", "Home", "_Video_Share_Twitter") });
		
		this._$btnTwitter = this.$dom.find('.btn--share-twitter');
		this._$btnTwitter.on(MouseEvent.CLICK, function() { ga("send", "event", "Home", "_Video_Share_Facebook") });

		SocialShare.popup(this._$btnFacebook, 'facebook');
		SocialShare.popup(this._$btnTwitter, 'twitter');
	},

	destroy: function()
	{
		VideoControls.prototype.destroy.call(this);

		this._$btnQualitySelector.off();
		this._$btnQualities.off();
		this._$btnFacebook.off();
		this._$btnTwitter.off();
	},

	_showQualityBtn: function()
	{
		this._$btnQualities.animate({opacity:1, visibility: 'visible'}, 700, Easing.outExpo);
	},

	_hideQualityBtn: function()
	{
		this._$btnQualities.animate({opacity:0, visibility: 'hidden'}, 500, Easing.outExpo);
	},

	//-----------------------------------------------------o Controller handlers

	_onClickQualitySelector: function(e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();

		// if (this._$btnQualities.opened)
			// this._hideQualityBtn();
		// else
		this._showQualityBtn();
		$(window).one(MouseEvent.CLICK, $.proxy(this._hideQualityBtn, this)); // Better use Stage class

		// this._$btnQualities.opened = !this._$btnQualities.opened;

		this.$.trigger(VideoEvent.QUALITY_SELECTION);
	},

	_onClickQuality: function(e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();

		var target;

		target = $(e.currentTarget);
		this.quality = target.attr("data-value");
		this._$btnQualitySelector.html(target.html());

		this._hideQualityBtn();

		this.$.trigger(VideoEvent.QUALITY_SELECTED, [this.quality]);
	},
});