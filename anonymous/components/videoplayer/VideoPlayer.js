/* globals Event, module, require */
"use strict";

var VideoControls = require("./controls/VideoControls");
var VideoOverlay = require("./controls/VideoOverlay");
var Video = require("./video/Video");
var VideoEvent = require("./events/VideoEvent");

/**
 * VideoPlayer
 * @constructor
 * @param $dom
 */
var VideoPlayer = module.exports = function($dom, params)
{
	$.extend(this, {autoPlay: false, showControls: true}, params);

	this.$ = $(this);
	this.$dom = $dom;
	this._$video = this.$dom.find('.video');
	this._isPaused = true;

	this.id = Math.random();

	this.init();
	this.initEvents();
};

VideoPlayer.prototype =
{
	init: function()
	{
		this._video = new Video(this.$dom.find("video"));
		this._overlay = new VideoOverlay(this.$dom.find(".video__overlay"),{poster: !this.autoPlay});
		if(this.showControls)
		{
			this._controls = new VideoControls(this.$dom.find(".video__controller"));
		}
	},

	initEvents: function()
	{
		this.$dom
			.on(MouseEvent.MOVE, $.proxy(this._onMouseMove, this))
			.on(MouseEvent.ENTER, $.proxy(this._onMouseEnter, this))
			.on(MouseEvent.LEAVE, $.proxy(this._onMouseLeave, this));

		this._video.$
			.on(VideoEvent.LOADED_METADATA, $.proxy(this._onVideoMetadata, this))
			.one(VideoEvent.READY, $.proxy(this._onVideoReadyOnce, this))
			.on(VideoEvent.PLAY, $.proxy(this._onVideoPlay, this))
			.on(VideoEvent.PAUSE, $.proxy(this._onVideoPause, this))
			.on(VideoEvent.ENDED, $.proxy(this._onVideoEnded, this))
			.on(VideoEvent.MUTE, $.proxy(this._onVideoMute, this))
			.on(VideoEvent.UNMUTE, $.proxy(this._onVideoUnmute, this));

		this._overlay.$
			.on(MouseEvent.CLICK, $.proxy(this._onClickOverlay, this))
			.on(MouseEvent.ENTER, $.proxy(this._onOverlayMouseEnter, this))
			.on(MouseEvent.LEAVE, $.proxy(this._onOverlayMouseLeave, this));

		if(this._controls)
		{
			this._controls.$
				.on(VideoEvent.STOP, $.proxy(this._onControlsStop, this))
				.on(VideoEvent.TIMELINE_DOWN, $.proxy(this._onControlsTimelineDown, this))
				.on(VideoEvent.SEEKING, $.proxy(this._onControlsSeek, this))
				.on(VideoEvent.PLAY, $.proxy(this._onControlsPlay, this))
				.on(VideoEvent.TIMELINE_UP, $.proxy(this._onControlsTimelineUp, this))
				.on(VideoEvent.TOGGLE_FULLSCREEN, $.proxy(this._onControlsToggleFullscreen, this))
				.on(VideoEvent.TOGGLE_PLAY_PAUSE, $.proxy(this._onControlsTogglePause, this))
				.on(VideoEvent.TOGGLE_MUTE, $.proxy(this._onControlsToggleMute, this));
		}

		$(document).on(Event.FULLSCREEN + '.' + this.id, $.proxy(this._onFullscreenChange, this));
	},

	update: function()
	{
		if(this._controls && this._video.ready && !this._video.paused)
			this._controls.time(this._video.currentTime(), this._video.duration());
	},

	destroy: function()
	{
		this.$dom.off();

		this._video.destroy();
		this._overlay.destroy();

		if(this._controls)
			this._controls.destroy();
	},

	_showControls: function()
	{
		if (this._controls) this._controls.unminify();
		if (this._overlay) this._overlay.showControls();
	},

	_hideControls: function()
	{
		if(this._video.ready && !this._video.ended && !this._video.paused)
		{
			if (this._controls) this._controls.minify();
			if (this._overlay) this._overlay.hideControls();
		}
	},

	_setInterval: function()
	{
		this._interval = setTimeout($.proxy(this._hideControls, this), 1000);
	},

	//-----------------------------------------------------o Handlers

	_onMouseMove: function(e)
	{
		if (this._interval)
			clearInterval(this._interval);

		if(this._video.ready && !this._video.ended && !this._video.paused)
		{
			this._showControls();
			this._setInterval();
		}
	},

	_onMouseEnter: function(e)
	{
		if(this._video.ready && !this._video.ended)
		{
			if(this._controls)
				this._controls.unminify();
		}
	},

	_onMouseLeave: function(e)
	{
		if(this._video.ready && !this._video.ended)
		{
			if(this._controls && !this._video.paused)
				this._controls.minify();
		}
	},

	_onFullscreenChange: function(e)
	{
		if ((document.fullscreenElement && document.fullscreenElement !== null) || (!document.mozFullscreenElement && !document.webkitFullscreenElement))
		{
			this.$dom.removeClass('fullscreen');

			this._isFullscreen = false;
		}
		else
		{
			this.$dom.addClass('fullscreen');
		}
	},

	//-----------------------------------------------------o Video handlers

	_onVideoMetadata: function(e)
	{

	},

	_onVideoReadyOnce: function(e)
	{
		this._overlay.hide();

		// if(this._controls)
			// this._controls.show();

		this.$.trigger(VideoEvent.READY);
	},

	_onVideoPlay: function(e)
	{
		if(!this._overlay.hidden && this._video.ready)
		{
			if(this._controls)
			{
				this._controls.show();
			}
			this._overlay.hide();
		}
	
		this._setInterval();

		if(this._controls){
			this._controls.mode("playing");
		}

		this._overlay.togglePlayPauseTo('pause');

		// if(this._isPaused)
		// {
		// 	this._overlay.hideControls();
		// }
		this._isPaused = false;

		this.$.trigger(VideoEvent.PLAY);
	},

	_onVideoPause: function(e)
	{
		if(this._controls){
			this._controls.mode("paused");
		}
		this._overlay.togglePlayPauseTo('play');
		this._isPaused = true;
	},

	_onVideoEnded: function(e)
	{
		if(this._controls && !this._controls.dragging)
		{
			this._overlay.show();
			this._video.stop();
			this._controls.reset();
			this._isPaused = true;

			if (this._overlay)
				this._overlay.togglePlayPauseTo('play');
			this.$.trigger(VideoEvent.ENDED);
		}
		else if(!this._controls)
			this.$.trigger(VideoEvent.ENDED);
	},

	_onVideoMute: function(e)
	{
		if(this._controls)
			this._controls.mute('on');
	},

	_onVideoUnmute: function(e)
	{
		if(this._controls)
			this._controls.mute('off');
	},

	//-----------------------------------------------------o Overlay handlers

	_onClickOverlay: function(e)
	{
		if(!this._controls || !this._controls.dragging)
			this._video.togglePause();
	},

	_onOverlayMouseEnter: function(e)
	{
		this._overlay.showControls();
	},

	_onOverlayMouseLeave: function(e)
	{
		if(!this._isPaused)
		{
			this._overlay.hideControls();
		}
	},

	//-----------------------------------------------------o Controls handler

	_onControlsToggleFullscreen: function()
	{
		// var el = this._video.$dom[0];
		var el = this.$dom[0];
		if(this._isFullscreen)
		{
			if (document.cancelFullScreen)
				document.cancelFullScreen();
			else if (document.mozCancelFullScreen)
				document.mozCancelFullScreen();
			else if (document.webkitCancelFullScreen)
				document.webkitCancelFullScreen();

			this._isFullscreen = false;
		}
		else
		{
			if (el.requestFullScreen)
				el.requestFullScreen();
			else if (el.mozRequestFullScreen)
				el.mozRequestFullScreen();
			else if (el.webkitRequestFullScreen)
				el.webkitRequestFullScreen();

			this._isFullscreen = true;
		}
	},

	_onControlsPlay: function()
	{
		this._video.play();
	},

	_onControlsStop: function()
	{
		this._controls.mode("stopped");
		this._video.stop();
	},

	_onControlsTimelineDown: function()
	{
		if(!this._video.paused)
			this._resumeAfterSeek = true;
		else
			this._resumeAfterSeek = false;
		this._video.pause();
	},

	_onControlsSeek: function(e, percent)
	{
		var time = percent * this._video.duration();
		this._controls.time(time, this._video.duration());
		this._video.seek(time);
	},

	_onControlsTimelineUp: function()
	{
		if(this._resumeAfterSeek)
			this._video.play();
	},

	_onControlsTogglePause: function()
	{
		this._video.togglePause();
	},

	_onControlsToggleMute: function()
	{
		this._video.toggleMute();
	}
};