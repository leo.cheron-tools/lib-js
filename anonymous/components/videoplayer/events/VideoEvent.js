"use strict"; // jshint ignore: line

var VideoEvent = module.exports = function(){};

VideoEvent.PLAY              = "play";
VideoEvent.STOP              = "stop";
VideoEvent.ERROR             = "error";
VideoEvent.ENDED             = "ended";
VideoEvent.PAUSE             = "paused";
VideoEvent.SEEKING           = "seeking";
VideoEvent.MUTE              = "mute";
VideoEvent.UNMUTE            = "unmute";
VideoEvent.LOADED_METADATA   = "loadedmetadata";

VideoEvent.READY             = "videoready";
VideoEvent.LOAD_ERROR        = "loaderror";
VideoEvent.PROGRESS          = "videoprogress";
VideoEvent.BUFFERED          = "videobuffered";
VideoEvent.BUFFERING         = "videobuffering";
VideoEvent.TIMELINE_UP       = "timelineup";
VideoEvent.TIMELINE_DOWN     = "timelinedown";
VideoEvent.QUALITY_SELECTION = "qualityselect";
VideoEvent.QUALITY_SELECTED  = "qualityselected";
VideoEvent.TOGGLE_PLAY_PAUSE = "videoplaypause";
VideoEvent.TOGGLE_FULLSCREEN = "videofullscreen";
VideoEvent.TOGGLE_MUTE       = "soundtooglemute";