"use strict";

var SrtParser = require("lib/anonymous/components/videoplayer/subtitles/SrtParser");

/**
 * @author Leo Cheron
 * @constructor
 */
var Subtitles = module.exports = function($dom, subs)
{
	this.$dom = $dom;
	this.subs = SrtParser.fromSrt(subs, true);
	this.subsLength = this.subs.length;

	this.currentSub = null;
};

Subtitles.prototype.update = function(time) 
{
	var time = time * 1000 | 0;
	var subbed = false, i;

	// console.log("Subtitles", time);
	
	for (i = 0; i < this.subsLength; ++i) 
	{
		var sub = this.subs[i];
		if(time > sub.startTime && time < sub.endTime)
		{
			if(this.currentSub != sub.text)
				this.$dom.html(sub.text);
			
			this.currentSub = sub.text;

			subbed = true;
			break;
		}
	}

	if(!subbed && this.currentSub != "")
	{
		this.$dom.text("");
		this.currentSub = "";
	}
};

Subtitles.prototype.reset = function()
{
	this.$dom.text("");
};
