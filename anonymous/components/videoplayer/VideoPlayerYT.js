/* globals module, require */
"use strict"; // jshint ignore: line

var VideoPlayer = require("./VideoPlayer");
var VideoControlsYT = require("./controls/VideoControlsYT");
var VideoOverlay = require("./controls/VideoOverlay");
var VideoYT = require("./video/VideoYT");
var VideoEvent = require("./events/VideoEvent");
var Subtitles = require("lib/anonymous/components/videoplayer/subtitles/Subtitles");

/**
 * VideoPlayerYT
 * @constructor
 */
var VideoPlayerYT = module.exports = function($dom, params)
{
	$.extend(this, {autoPlay: false, showControls: true}, params);

	VideoPlayer.apply(this, arguments);
};

VideoPlayerYT.prototype = $.extend({}, VideoPlayer.prototype,
{
	init: function()
	{
		this._video = new VideoYT(this.$dom.find("[data-id]"));
		this._overlay = new VideoOverlay(this.$dom.find(".video__overlay"), {poster: !this.autoPlay});

		this._loadSrt();

		if(this.showControls)
			this._controls = new VideoControlsYT(this.$dom.find(".video__controller"));
			// this._controls.hide();

		this._video.$.on(VideoEvent.READY, $.proxy(this._onVideoPlayReady, this));

		if(this.autoPlay)
			this._onClickOverlay();
	},

	initEvents: function()
	{
		VideoPlayer.prototype.initEvents.call(this, arguments);

		if(this.showControls)
		{
			this._controls.$
				.on(VideoEvent.QUALITY_SELECTED, $.proxy(this._onClickQuality, this));
		}
	},

	destroy: function()
	{
		VideoPlayer.prototype.destroy.call(this);
	},

	update: function()
	{
		VideoPlayer.prototype.update.call(this);

		if(this._subtitles && this._video.ready)
			this._subtitles.update(this._video.currentTime());
	},

	changeTo: function(id, startSeconds, suggestedQuality)
	{
		this._video.loadVideoById(id, startSeconds, suggestedQuality);
	},

	//-----------------------------------------------------o private

	_loadSrt: function()
	{
		if(this.srt)
		{
			var request = new XMLHttpRequest();			
			request.onreadystatechange = $.proxy(function()
			{
				if (request.readyState === 4 && request.status === 200)
				{
					this._subtitles = new Subtitles(this.$dom.find(".video__subs"), request.responseText);
				}
			}, this);

			request.open('GET', this.srt);
			request.send();
		}
	},

	//-----------------------------------------------------o Controls handlers

	_onClickQuality: function(e, quality)
	{
		this._video.quality(quality);
	},

	//-----------------------------------------------------o Overlay handlers

	_onClickOverlay: function(e)
	{
		if ( $.os.phone )
		{
			this._overlay.hide();
			this._overlay.hideControls();

			if(this.showControls)
			{
				this._controls.show();
				this._controls.hideForMobile();
			}
			
		}

		if (!this._video.video)
		{
			this._video.paused = false;
			this._video.initYT();
		}
		else
			VideoPlayer.prototype._onClickOverlay.call(this);
	},

	_onVideoPlayReady: function()
	{
		this.$dom.addClass('ready');
	},
});