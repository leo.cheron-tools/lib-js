/* globals module, require */
"use strict"; // jshint ignore: line

var Input = require("lib/anonymous/components/input/Input");

/**
 * InputSelect
 * @constructor
 */
var InputSelect = module.exports = function($dom)
{
	Input.apply(this, arguments);
};

InputSelect.prototype = $.extend({}, Input.prototype,
{
	_init: function()
	{
		Input.prototype._init.apply(this);

		this._initValues();
	},

	_isValid: function()
	{
		if ( this.$dom.prop('required') && this._values.indexOf(this.$dom.val()) > -1 )
			return true;
		else
			return false;

		return true;
	},

	_initValues: function()
	{
		var $opt = this.$dom.find('option');

		var values = [];

		$opt.each(function()
		{
			if (!this.disabled)
			{
				values.push(this.value);
			}
		});

		this._values = values;
	},

});