/* globals module, w, require */
"use strict"; // jshint ignore: line

var Config = require("Config");
var Input = require("lib/anonymous/components/input/Input");
var FormErrorCode = require("lib/anonymous/components/form/FormErrorCode");

/**
 * InputBirthdate
 * @constructor
 */
var InputBirthdate = module.exports = function($dom)
{
	this._support = undefined;
	Input.apply(this, arguments);
};

InputBirthdate.prototype = $.extend({}, Input.prototype,
{
	_init: function()
	{
		Input.prototype._init.apply(this);
		this._isEmpty();

		this._checkSupport();

		if (!this._support && this.$dom.val())
			this._formatValue(true);
	},

	destroy: function()
	{
		Input.prototype.destroy.apply(this);
	},

	_checkSupport: function()
	{
		var oValue = this.$dom.val();
		var notADateValue = 'not-a-date';

		this.$dom.val(notADateValue);
		this._support = this.$dom[0].value !== notADateValue;
		this.$dom.val(oValue);
	},

	_isEmpty: function()
	{
		if ( !this.$dom.val() )
			this.$dom.addClass('empty');
		else
			this.$dom.removeClass('empty');	
	},

	_formatValue: function(isInit, joinString)
	{
		if (!joinString) joinString = '/';

		var v;

		this._oldVal = this.$dom.val();
		
		v = this._oldVal;
		if (isInit)
			v = v.split('-');
		else
			v = v.split('/');
w
		if (isInit)
			if (Config.LANG === 'fr')
				v = v.reverse();
			else
				v = [ v[1],v[2],v[0] ];
		else
			if (Config.LANG === 'fr')
				v = v.reverse();
			else
				v = [ v[2],v[0],v[1] ];

		v = v.join(joinString);

		this.$dom.val(v);

		if (isInit)
			this._oldVal = v;
	},

	_revertValue: function()
	{
		if (this._oldVal)
			this.$dom.val(this._oldVal);
	},

	//-----------------------------------------------------o Override

	_isValid: function()
	{
		var v,
			valid;

		if (this._support)
		{
			valid = this.$dom[0].checkValidity();
			console.log('valid',valid);
		}
		else
		{
			this._formatValue();

			v = this.$dom.val();
			valid = /^[0-9]{4}\/[0-9]{2}\/[0-9]{2}/.test(v);
			valid = valid && !isNaN( new Date(v) );

			this._revertValue();
		}

		if (v && !valid) this.addTextError(FormErrorCode.INVALID);

		return valid;
	},

	_onChange: function()
	{
		this._removeError();
		this._isEmpty();
	},

	_beforeSend: function()
	{
		if (!this._support)
			this._formatValue(null, '-');
	},
	
	_afterSend: function()
	{
		if (!this._support)
			this._revertValue();
	},

});