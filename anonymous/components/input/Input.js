/* globals InputEvent, module */
"use strict"; // jshint ignore: line

/**
 * Input
 * @constructor
 */
var Input = module.exports = function($dom)
{
	this.$ = $(this);
	
	this.$dom = $dom;

	this.name = this.$dom[0].name;
	
	this._init();
};

Input.ERRORCLASS = 'error';

Input.prototype =
{
	_init: function()
	{
		this.$dom.on(InputEvent.CHANGE + ' ' + InputEvent.FOCUS, $.proxy(this._onChange, this));
	},

	destroy: function()
	{
		this.$dom.off(InputEvent.CHANGE, $.proxy(this._onChange, this));
	},

	isValid: function()
	{
		if ( !this._isValid() )
		{
			this.addError();
			return false;
		}
		return true;
	},

	_isValid: function()
	{
		return this.$dom[0].checkValidity();
	},

	addTextError: function(code)
	{
		var msg = this.$dom.data('error-' + code);
		if (msg)
		{
			this.$dom.next().html(msg);
		}
	},

	addError: function()
	{
		this.$dom.addClass(Input.ERRORCLASS);
	},

	_onChange: function()
	{
		this._removeError();
	},

	_removeError: function()
	{
		this.$dom.removeClass(Input.ERRORCLASS);
		this.$dom.next('.error').html('');
	},

	_beforeSend: function()
	{
		
	},
	
	_afterSend: function()
	{
		
	},
};