/* globals module, require */
"use strict"; // jshint ignore: line

var Input = require("lib/anonymous/components/input/Input");
// var InputRadio = require("lib/anonymous/components/input/InputRadio");

/**
 * RadioGroup
 * @constructor
 */
var RadioGroup = module.exports = function($dom)
{
	Input.apply(this, arguments);
};

RadioGroup.prototype = $.extend({}, Input.prototype,
{
	_init: function()
	{
		Input.prototype.init.apply(this);
	},

	destroy: function()
	{
		Input.prototype.destroy.apply(this);
	},

	isValid: function()
	{
		if (!this._isValid())
		{
			this._addError();
			return false;
		}
		return true;
	},

	_isValid: function()
	{
		return this.$dom[0].checkValidity();
	},

	_addError: function()
	{
		this.$dom.addClass(Input.ERRORCLASS);
	},

	_removeError: function()
	{
		this.$dom.removeClass(Input.ERRORCLASS);
	},
});