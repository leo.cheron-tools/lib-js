/* globals module, require */
"use strict"; // jshint ignore: line

var Input = require("lib/anonymous/components/input/Input");

/**
 * InputCheckbox
 * @constructor
 */
var InputCheckbox = module.exports = function($dom)
{
	Input.apply(this, arguments);
};

InputCheckbox.prototype = $.extend({}, Input.prototype,
{
	init: function()
	{
		Input.prototype.init.apply(this);
	},

	destroy: function()
	{
		Input.prototype.destroy.apply(this);
	},

	_isValid: function()
	{
		return this.$dom[0].checkValidity();
	},

	_addError: function()
	{
		this.$dom.addClass(Input.ERRORCLASS);
	},

	_removeError: function()
	{
		this.$dom.removeClass(Input.ERRORCLASS);
	},

});